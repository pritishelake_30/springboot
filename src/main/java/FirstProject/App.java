package FirstProject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
//Dependency Injection by using Setter Injection
public class App {
  public static void main(String[] args) {
    //System.out.println("Hello World!");
    ApplicationContext con=new ClassPathXmlApplicationContext("FirstProject/Config.xml");
    student s1=(student) con.getBean("student1");
    System.out.println(s1);
  }
}
