package Constructor_Ambiguity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("Constructor_Ambiguity/ConstAmbiguity.xml");
		Addition ad= (Addition) context.getBean("add");
		ad.sum();//by default it will call constructor with parameter string, string, otherwise top most constructor
		

	}

}
