package com.test.services;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.test.entities.Book;
@Component
public class BookService 
{
	private static List<Book> l1=new ArrayList<>();
	static 
	{
		l1.add(new Book(12,"java","XYZ"));
		l1.add(new Book(13,"python","ABC"));
		l1.add(new Book(14,"Dot net","LMN"));
	}
	// get all books
	public List<Book> getAllBooks()
	{
		return l1;
	}
	//get single book by id
	public Book getBookById(int id)
	{
		Book b=null;
		b=l1.stream().filter(e->e.getId()==id).findFirst().get();
		return b;
		
	}
	//add book
	public Book addBook(Book b)
	{
		l1.add(b);
		return b;
	}
	//delete book
	public void deleteBook(int bId)
	{
		l1=l1.stream().filter(book->book.getId()!=bId).collect(Collectors.toList());
	}
	//update book
	public void updateBook(Book book, int bookId)
	{
		l1=l1.stream().map(b->
		
		{
			if(b.getId()==bookId)
			{
				b.setTitle(book.getTitle());
				b.setAuthor(book.getAuthor());
			}
			return b;
		}).collect(Collectors.toList());
	}
	

}
