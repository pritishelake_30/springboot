package Constructor_Injection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext c1=new ClassPathXmlApplicationContext("Constructor_Injection/CnstructorConfig.xml");
		person p1=(person) c1.getBean("person1");
		System.out.println(p1);

	}

}
