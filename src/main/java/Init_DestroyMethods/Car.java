package Init_DestroyMethods;
public class Car 
{
	private int price;

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		System.out.println("Setting price");
		this.price = price;
	}

	public Car(int price) {
		super();
		this.price = price;
	}

	public Car() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Car [price=" + price + "]";
	}
	public void Init() 
	{
		System.out.println("Init() method after properties are set "+price);
	}
	public void distroy() 
	{
		System.out.println("Spring Container is destroy ! Customer clean up");
	}
	

}
