package com.jpa.test;// CRUD operations using jpa & spring Boot

import java.util.List;
import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.jpa.test.dao.UserRepository;
import com.jpa.test.entities.User;

@SpringBootApplication
public class BootJpaExampleApplication {

	public static void main(String[] args) {
		ApplicationContext con=SpringApplication.run(BootJpaExampleApplication.class, args);
		UserRepository repo=con.getBean(UserRepository.class);
		// create object of user
		User user1=new User();
		user1.setId(1);
		user1.setName("XYZ");
		user1.setCity("Delhi");
		user1.setStatus("I am  java programmer");
		//saving single user
		User user2=repo.save(user1);
		System.out.println(user2);
		User user3=new User();
		user3.setId(2);
		user3.setName("Abc");
		user3.setCity("Mumbai");
		user3.setStatus("I am  Dot net programmer");
		User user4=repo.save(user3);
		//System.out.println(user4);
		
		//To update
		
		Optional<User> opt=repo.findById(2);
		User u1=opt.get();
		u1.setName("Pqr");
		User u2=repo.save(u1);
		System.out.println(u2);
		//Deleting the user element
		//repo.deleteById(4);
		//System.out.println("Deleted");
		// Custom finder methods
		List<User> u=repo.findByNameAndCity("Shital Kate","solapur");
		u.forEach(e->System.out.println(e));
		
		
		
		
		
		
	}
	

}
